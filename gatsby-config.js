import { join } from 'path';

module.exports = {
	siteMetadata: {
		title: `Nakada Link - Chainlink Oracle Nodes`,
		description: `Reliable Chainlink oracles for smart contract developers`,
		author: `oyabun@nakada.link`,
	},
	plugins: [
		`gatsby-plugin-react-helmet`,
		"gatsby-plugin-eslint",
		`gatsby-transformer-sharp`,
		`gatsby-plugin-sharp`,
		`gatsby-plugin-offline`,
		`gatsby-plugin-material-ui`,
		`gatsby-transformer-json`,
		{
			resolve: `gatsby-plugin-root-import`,
			options: {
				src: join(__dirname, `src`),
			},
		},
		{
			resolve: `gatsby-plugin-import`,
			options: {
				libraryName: `@material-ui/core`,
				libraryDirectory: `esm`,
				camel2DashComponentName: false
			}
		},
		{
			resolve: `gatsby-source-filesystem`,
			options: {
				name: `images`,
				path: `${__dirname}/src/images`,
			},
		},
		{
			resolve: `gatsby-source-filesystem`,
			options: {
				name: `data`,
				path: `${__dirname}/src/data`,
			},
		},
		{
			resolve: `gatsby-plugin-manifest`,
			options: {
				name: `Grant Jordan Portfolio Site`,
				short_name: `codebyjordan.com`,
				start_url: `/`,
				background_color: `#663399`,
				theme_color: `#663399`,
				display: `minimal-ui`,
				icon: `src/images/cbj-logo-round.png`,
			},
		},
		{
			resolve: `gatsby-plugin-prefetch-google-fonts`,
			options: {
				fonts: [
					{
						family: `Roboto`,
						variants: [`300`, `400`, `500`, `700`],
					},
				],
			},
		},
	],
}
